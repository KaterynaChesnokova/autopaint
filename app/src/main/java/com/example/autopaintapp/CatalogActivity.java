package com.example.autopaintapp;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;


public class CatalogActivity extends BaseActivity{
    DataProvider dataProvider = new DataProvider();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.catalog_page);

        ArrayList<MyItem> items = dataProvider.getItems();

        ListView listView = (ListView)findViewById(R.id.catalog_list);
        CatalogAdapter adapter = new CatalogAdapter(this, items);
        listView.setAdapter(adapter);
    }

    private class CatalogAdapter extends ArrayAdapter<MyItem> {

        public CatalogAdapter(Context context, ArrayList<MyItem> items){
            super(context,R.layout.catalog_item,items);
        }

        @NonNull
        @Override
        public View getView(int i, @Nullable View view, @NonNull ViewGroup viewGroup) {
            MyItem item=getItem(i);

            if(view == null){
                view = LayoutInflater.from(getContext()).inflate(R.layout.catalog_item,viewGroup,false);
            }

            TextView nameTextview=view.findViewById(R.id.nameTextview);
            nameTextview.setText(item.name);

            ImageView imageView=view.findViewById(R.id.imageView);
            imageView.setImageResource(item.img);

            TextView descTextview=view.findViewById(R.id.descTextview);
            descTextview.setText(item.description);

            TextView priceTextview=view.findViewById(R.id.priceTextview);
            priceTextview.setText(Double.toString(item.price));

            EditText numberTextbox = (EditText)view.findViewById(R.id.numberTextbox);
            if(numberTextbox != null)
                numberTextbox.setId(item.id);

            Button toCartButton = (Button)view.findViewById(R.id.toCartButton);
            toCartButton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View view) {
                    EditText numberTextbox =  (EditText)findViewById(item.id);
                    int itemCount = Integer.parseInt(numberTextbox.getText().toString());

                    addItemToCart(item.id, itemCount);
                }
            });

            return view;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        private void addItemToCart(int itemId, int selectedCount) {
            dataProvider.addCartItem(itemId, selectedCount);
            Toast.makeText(getBaseContext(),"Додано в кошик",Toast.LENGTH_LONG).show();
        }
    }

}
