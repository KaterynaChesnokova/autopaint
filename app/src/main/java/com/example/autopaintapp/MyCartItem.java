package com.example.autopaintapp;

public class MyCartItem {
    String name;
    double price;
    int id, img,count;

    public MyCartItem(int id, String name, double price, int img, int count) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
        this.count = count;
    }
}
