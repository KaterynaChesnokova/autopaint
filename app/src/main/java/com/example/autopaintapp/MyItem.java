package com.example.autopaintapp;

public class MyItem {
    String name, description;
    double price;
    int id, img;

    public MyItem(int id,String name, String description, double price, int img) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.img = img;
    }
}
