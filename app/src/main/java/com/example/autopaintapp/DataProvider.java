package com.example.autopaintapp;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;

public class DataProvider {
    private static ArrayList<MyUser> users = new ArrayList<MyUser>();
    private static ArrayList<MyItem> items = new ArrayList<MyItem>();
    private static ArrayList<MyCartItem> cartItems = new ArrayList<MyCartItem>();
    private static MyUser currentUser;

    static {
        users.add(new MyUser("Наталія","Іванова","password1","natalia@gmail.com","1234"));
        users.add(new MyUser("Сергій","Мельник","password2","sergii@gmail.com","1235"));

        items.add(new MyItem(1, "Red paint","Red paint",200, R.drawable.a));
        items.add(new MyItem(2, "Green paint","Green paint",168, R.drawable.b));
        items.add(new MyItem(3, "Yellow paint","Yellow paint",132, R.drawable.c));
        items.add(new MyItem(4, "Blue paint","Blue paint",271, R.drawable.d));
        items.add(new MyItem(5, "Pink paint","Pink paint",253, R.drawable.e));
        items.add(new MyItem(6, "Purple paint","Purple paint",154, R.drawable.f));
    }

    public MyUser getCurrentUser(){
        return currentUser;
    }

    public void setCurrentUser(MyUser newCurrentUser){
        currentUser = newCurrentUser;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean userLogin(String login, String password){
        MyUser newCurrentUser = findUser(login);
        if(newCurrentUser == null){
            return false;
        }
        else{
            currentUser = newCurrentUser;
            return true;
        }
    }

    public void addUser(MyUser user){ users.add(user); }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public MyUser findUser(String login) {

        return users.stream().filter(x -> x.email.equals(login)).findAny().orElse(null);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void updateUser(MyUser user) {
        MyUser updateUser = this.findUser(user.email);
        updateUser.firstName = user.firstName;
        updateUser.secondame = user.secondame;
        updateUser.email = user.email;
        updateUser.password = user.password;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean userExists(String login){
        return users.stream().anyMatch(x -> x.email == login);
    }

    public ArrayList<MyItem> getItems(){
        return items;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public MyItem getItem(int id) { return items.stream().filter(x -> x.id == id).findAny().orElse(null); }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public MyCartItem getCartItem(int id) { return cartItems.stream().filter(x -> x.id == id).findAny().orElse(null); }

    public ArrayList<MyCartItem> getCartItems(){
        return cartItems;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void addCartItem(int id, int count){
        MyItem item = this.getItem(id);
        MyCartItem cartItem = cartItems.stream().filter(x -> x.id == id).findAny().orElse(null);

        if(cartItem != null){
            cartItem.count += count;
        }
        else {
            cartItems.add(new MyCartItem(id, item.name, item.price, item.img, count));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void removeCartItem(int id){
        cartItems.remove(getCartItem(id));
    }
}
