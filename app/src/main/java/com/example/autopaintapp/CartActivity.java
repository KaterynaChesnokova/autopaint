package com.example.autopaintapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CartActivity extends BaseActivity{
    Button buyButton;
    DataProvider dataProvider = new DataProvider();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_page);

        ArrayList<MyCartItem> cartItems = dataProvider.getCartItems();

        ListView listView = (ListView)findViewById(R.id.cart_list);
        CartAdapter adapter = new CartAdapter(this, cartItems);
        listView.setAdapter(adapter);

        buyButton = (Button)findViewById(R.id.cart_buyButton);
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openOrderActivity();
            }
        });

        double calcTotalPrice = 0;
        for (int k=0;k<cartItems.size();k++){
            calcTotalPrice += cartItems.get(k).price * cartItems.get(k).count;
        }

        TextView totalTextView = findViewById(R.id.card_totalTextView);
        totalTextView.setText("Всього " + Double.toString(calcTotalPrice));
    }

    private void openOrderActivity() {
        Intent intent = new Intent(this, OrderActivity.class);
        startActivity(intent);
    }

    private class CartAdapter extends ArrayAdapter<MyCartItem> {
        public CartAdapter(Context context, ArrayList<MyCartItem> items){
            super(context,R.layout.cart_item,items);
        }

        @Override
        public long getItemId(int i) {
            return ((long) i);
        }

        @NonNull
        @Override
        public View getView(int i, @Nullable View view, @NonNull ViewGroup viewGroup) {
            MyCartItem item=getItem(i);
            ArrayList<MyCartItem> cartItems = dataProvider.getCartItems();

            if(view == null){
                view = LayoutInflater.from(getContext()).inflate(R.layout.cart_item,viewGroup,false);
            }

            ImageView imageView=view.findViewById(R.id.cart_imageView);
            imageView.setImageResource(item.img);

            TextView nameTextview=view.findViewById(R.id.cart_nameTextview);
            nameTextview.setText(item.name);

            TextView priceTextview=view.findViewById(R.id.cart_calcTextView);
            priceTextview.setText(Double.toString(item.price) + " грн x " + Integer.toString(item.count));

            TextView calcTextView=view.findViewById(R.id.calc_sumTextView);
            calcTextView.setText(Double.toString(item.price * item.count));

            return view;
        }
    }
}
