package com.example.autopaintapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PaymentActivity extends BaseActivity{
    private Button payButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_page);

        payButton = (Button)findViewById(R.id.payButton);
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //change payment status
                Toast.makeText(getBaseContext(),"Сплачено!",Toast.LENGTH_LONG).show();
            }
        });
    }
}
