package com.example.autopaintapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class RegistrationActivity extends AppCompatActivity {

    private DataProvider dataProvider = new DataProvider();
    private Button registerButton;
    private Button accExistsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_page);

        registerButton = (Button)findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                EditText loginTextbox =  (EditText)findViewById(R.id.loginTextbox);
                EditText phoneTextbox =  (EditText)findViewById(R.id.phoneTextbox);
                EditText passwordTextbox =  (EditText)findViewById(R.id.passwordTextbox);

                String login = loginTextbox.getText().toString();
                String phone = phoneTextbox.getText().toString();
                String password = passwordTextbox.getText().toString();


                if(dataProvider.findUser(login) == null){
                    if(password.equals("") || login.equals("") || phone.equals("")) {
                        Toast.makeText(getBaseContext(), "Невірні дані", Toast.LENGTH_LONG).show();
                    }
                    else {
                        MyUser user = new MyUser("", "", password, login, phone);
                        dataProvider.addUser(user);
                        dataProvider.setCurrentUser(user);
                        openCatalogActivity();
                    }
                }
                else{
                    Toast.makeText(getBaseContext(),"Користувач вже створений",Toast.LENGTH_LONG).show();
                }
            }
        });

        accExistsButton = (Button)findViewById(R.id.accExistsButton);
        accExistsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMainActivity();
            }
        });
    }

    private void openCatalogActivity() {
        Intent intent = new Intent(this, CatalogActivity.class);
        startActivity(intent);
    }

    private void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
