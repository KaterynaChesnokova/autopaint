package com.example.autopaintapp;

public class MyUser {
    String firstName, secondame, password, email, phone;

    public MyUser(String firstName, String secondame, String password, String email, String phone) {
        this.firstName = firstName;
        this.secondame = secondame;
        this.password = password;
        this.email = email;
        this.phone = phone;
    }
}
