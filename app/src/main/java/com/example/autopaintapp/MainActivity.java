package com.example.autopaintapp;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button loginButton;
    private Button newAccButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        DataProvider dataProvider = new DataProvider();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton = (Button)findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                EditText loginTextbox =  (EditText)findViewById(R.id.loginTextbox);
                EditText passwordTextbox =  (EditText)findViewById(R.id.passwordTextbox);

                String login = loginTextbox.getText().toString();
                String password = passwordTextbox.getText().toString();

                boolean loggedIn = dataProvider.userLogin(login, password);
                if(!loggedIn){
                    Toast.makeText(getBaseContext(),"Невірний логін або пароль",Toast.LENGTH_LONG).show();
                }
                else{
                    openCatalogActivity();
                }
            }
        });

        newAccButton = (Button)findViewById(R.id.newAccButton);
        newAccButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openRegistrationActivity();
            }
        });
    }

    private void openCatalogActivity() {
        Intent intent = new Intent(this, CatalogActivity.class);
        startActivity(intent);
    }

    public  void openRegistrationActivity(){
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }
}