package com.example.autopaintapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

public class UserAccountActivity extends BaseActivity{
    DataProvider dataProvider = new DataProvider();

    Button buttonExit;
    Button buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_account_page);

        MyUser currentUser = dataProvider.getCurrentUser();

        EditText firstNameTextView =  (EditText)findViewById(R.id.firstNameTextView);
        firstNameTextView.setText(currentUser.firstName);

        EditText secondNameTextView =  (EditText)findViewById(R.id.secondNameTextView);
        secondNameTextView.setText(currentUser.secondame);

        EditText loginTextView =  (EditText)findViewById(R.id.emailTextView);
        loginTextView.setText(currentUser.email);

        EditText passwordTextView =  (EditText)findViewById(R.id.emailTextView2);
        passwordTextView.setText(currentUser.password);

        buttonExit = (Button)findViewById(R.id.buttonExit);
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMainActivity();
            }
        });

        buttonSave = (Button)findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                String firstName = firstNameTextView.getText().toString();
                String secondName = secondNameTextView.getText().toString();
                String login = loginTextView.getText().toString();
                String password = passwordTextView.getText().toString();

                MyUser newUserData = new MyUser(firstName, secondName,password,login,"");

                dataProvider.updateUser(newUserData);

                if(loginTextView.equals("") || passwordTextView.equals("")){
                    Toast.makeText(getBaseContext(),"Дані невірні", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getBaseContext(), "Зміни збережено", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
